package com.grizzlynetwork.boosters;

import com.grizzlynetwork.boosters.cmds.BoosterCMD;
import com.grizzlynetwork.boosters.configuration.ConfigManager;
import com.grizzlynetwork.boosters.configuration.MessageConfiguration;
import com.grizzlynetwork.boosters.listeners.*;
import com.grizzlynetwork.boosters.player.PlayerManager;
import com.grizzlynetwork.boosters.util.SQLHandler;
import com.grizzlynetwork.boosters.util.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class Boosters extends JavaPlugin{

    @Getter private PlayerManager playerManager;
    @Getter private ConfigManager configManager;
    @Getter private MessageConfiguration messageConfig;
    @Getter private GrowthHandler growthHandler;
    @Getter private Utils utils;
    @Getter private SQLHandler sqlHandler;
    @Getter private boolean debug = false;

    @Override
    public void onEnable(){
        this.configManager = new ConfigManager(this);
        this.sqlHandler = new SQLHandler(this);
        this.messageConfig = new MessageConfiguration(this);
        this.utils = new Utils(this);
        this.playerManager = new PlayerManager(this);
        this.configManager.load();
        this.growthHandler = new GrowthHandler(this);
        registerCMDs();
        registerEvents();
        this.growthHandler.runTaskTimer(this, 0L, 20L);
        new Tracker(this).runTaskTimer(this, 0L, 20L);
    }


    @Override
    public void onDisable(){
        //SAVE ALL BOOSTERS
        for (Player player : Bukkit.getServer().getOnlinePlayers()){
            this.sqlHandler.saveAllBoostersSync(player.getUniqueId());
        }
        this.sqlHandler.close();

    }


    private void registerCMDs(){
        getCommand("booster").setExecutor(new BoosterCMD(this));
    }

    private void registerEvents(){
        PluginManager plm = Bukkit.getPluginManager();
        plm.registerEvents(new BlockHandler(this), this);
        plm.registerEvents(new ExperienceHandler(this), this);
        plm.registerEvents(this.growthHandler, this);
        plm.registerEvents(new MoneyHandler(this), this);
        plm.registerEvents(new InteractHandler(this), this);
        plm.registerEvents(new LoginHandler(this), this);
    }


    public void log(String message){
        if (this.debug){
            Bukkit.getLogger().log(Level.INFO, "[Boosters] >> " + message);
        }
    }
}
