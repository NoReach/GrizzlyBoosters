package com.grizzlynetwork.boosters.cmds;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.Booster;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import com.grizzlynetwork.boosters.util.TimeAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BoosterCMD implements CommandExecutor {

    private Boosters boosters;

    public BoosterCMD(Boosters boosters) {
        this.boosters = boosters;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("boosters.give")){
            sender.sendMessage(this.boosters.getMessageConfig().getNO_PERMISSIONS_MSG());
            return true;
        }

        try{
            if (!args[0].equalsIgnoreCase("give")){
                throw new ArrayIndexOutOfBoundsException();
            }
            Player target = Bukkit.getPlayer(args[1]);
            if (target == null){
                throw new ArrayIndexOutOfBoundsException();
            }
            if (!args[2].equalsIgnoreCase("sell") && !args[2].equalsIgnoreCase("growth") && !args[2].equalsIgnoreCase("exp")){
                throw new ArrayIndexOutOfBoundsException();
            }
            int amount;
            try{
                amount = Integer.parseInt(args[3]);
            }catch (NumberFormatException e){
                throw new ArrayIndexOutOfBoundsException();
            }
            int multiplier;
            try{
                multiplier = Integer.parseInt(args[4]);
            }catch (NumberFormatException e){
                throw new ArrayIndexOutOfBoundsException();
            }
            String durationAsString = args[5];
            double asSeconds = new TimeAPI(durationAsString).getSeconds();
            Booster playerObject = new Booster(target, BoosterTypes.valueOf(args[2].toUpperCase()), amount, multiplier, asSeconds);
            playerObject.give(this.boosters, durationAsString);
            playerObject = null;
            asSeconds = -1;
            durationAsString = null;
            return true;
        }catch (ArrayIndexOutOfBoundsException e){
            sender.sendMessage(ChatColor.RED + "Usage: /booster give <player> <booster> <amount> <multiplier> <duration>");
            sender.sendMessage(ChatColor.RED + "Usage: Duration must be in the format 1h30m for an 1.5 hours, and 30m for 30 min etc.");
        }
        return false;
    }
}
