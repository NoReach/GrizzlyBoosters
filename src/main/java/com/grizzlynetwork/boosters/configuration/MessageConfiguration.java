package com.grizzlynetwork.boosters.configuration;

import com.grizzlynetwork.boosters.Boosters;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class MessageConfiguration {

    private Boosters boosters;

    public MessageConfiguration(Boosters boosters) {
        this.boosters = boosters;
        this.init();
    }

    @Getter private String GIVE_BOOSTER_MSG = "";
    @Getter private String NO_PERMISSIONS_MSG = "";
    @Getter private String BOOSTER_ACTIVATED = "";
    @Getter private String BOOSTER_EXPIRED = "";
    @Getter private String BOOSTER_ALREADY_ACTIVE = "";
    @Getter private String CANNOT_PICKUP = "";
    @Getter private String INV_FULL = "";
    @SneakyThrows
    private void init(){
        YamlConfiguration config = YamlConfiguration.loadConfiguration(new File(this.boosters.getDataFolder() + File.separator + "messages.yml"));

        config.addDefault("Messages.givenBooster", "You've recieved a %TYPE% with a %MULTIPLIER% and a duration of %DURATION%");
        config.addDefault("Messages.noPermissions", "&cYou do not have permission to use this command!");
        config.addDefault("Messages.boosterActivated", "You've activated the %TYPE% booster with a %MULTIPLIER% for %DURATION%");
        config.addDefault("Messages.boosterExpired", "Your %TYPE% booster has expired!");
        config.addDefault("Messages.boosterTypeAlreadyActive", "You already have this booster type enabled!");
        config.addDefault("Messages.cannotPickupBooster", "You cannot pickup this booster as your inventory is full!");
        config.addDefault("Messages.fullInventory", "You currently have a full inventory and cannot receive this booster!, dropping it on the floor!");
        config.options().copyDefaults(true);
        config.save(this.boosters.getDataFolder() + File.separator + "messages.yml"); //SAVES THE CONFIG



        this.GIVE_BOOSTER_MSG = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.givenBooster"));
        this.NO_PERMISSIONS_MSG = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.noPermissions"));
        this.BOOSTER_ACTIVATED = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.boosterActivated"));
        this.BOOSTER_EXPIRED = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.boosterExpired"));
        this.BOOSTER_ALREADY_ACTIVE = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.boosterTypeAlreadyActive"));
        this.CANNOT_PICKUP = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.cannotPickupBooster"));
        this.INV_FULL = ChatColor.translateAlternateColorCodes('&', config.getString("Messages.fullInventory"));
    }
}
