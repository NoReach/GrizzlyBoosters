package com.grizzlynetwork.boosters.configuration;

import com.google.common.collect.ImmutableList;
import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.util.ItemCreator;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ConfigManager {

    private Boosters boosters;

    public ConfigManager(Boosters boosters) {
        this.boosters = boosters;
        this.populate();
    }

    @Getter private ItemStack SELL_ITEM = null;
    @Getter private ItemStack EXP_ITEM = null;
    @Getter private ItemStack GROWTH_ITEM = null;


    @Getter private String SQL_IP = "";
    @Getter private int SQL_PORT = 3306;
    @Getter private String SQL_DATABASE = "";
    @Getter private String SQL_USERNAME = "";
    @Getter private String SQL_PASSWORD = "";

    @SneakyThrows
    private void populate() {
        //DUMMY LORE TO USE AS A PLACEHOLDER
        List<String> dummyLore = new ArrayList<>();
        dummyLore.add("&cLine One %MULTIPLIER%");
        dummyLore.add("&cLine two %DURATION%");
        dummyLore.add("&cLine three");
        dummyLore.add("&cLine four");


        //SQL Settings
        this.boosters.getConfig().addDefault("Settings.SQL.ip", "localhost");
        this.boosters.getConfig().addDefault("Settings.SQL.port", 3306);
        this.boosters.getConfig().addDefault("Settings.SQL.database", "grizzly_data");
        this.boosters.getConfig().addDefault("Settings.SQL.username", "username");
        this.boosters.getConfig().addDefault("Settings.SQL.password", "password");

        //Item Settings
        this.boosters.getConfig().addDefault("ItemSettings.SellBooster.Name", "&7[&cSellBooster&7]");
        this.boosters.getConfig().addDefault("ItemSettings.SellBooster.Lore", dummyLore);
        this.boosters.getConfig().addDefault("ItemSettings.SellBooster.ItemMaterial", Material.PAPER.toString());

        this.boosters.getConfig().addDefault("ItemSettings.ExpBooster.Name", "&7[&cExpBooster&7]");
        this.boosters.getConfig().addDefault("ItemSettings.ExpBooster.Lore", ImmutableList.copyOf(dummyLore));
        this.boosters.getConfig().addDefault("ItemSettings.ExpBooster.ItemMaterial", Material.OBSIDIAN.toString());

        this.boosters.getConfig().addDefault("ItemSettings.GrowthBooster.Name", "&7[&cGrowthBooster&7]");
        this.boosters.getConfig().addDefault("ItemSettings.GrowthBooster.Lore", ImmutableList.copyOf(dummyLore));
        this.boosters.getConfig().addDefault("ItemSettings.GrowthBooster.ItemMaterial", Material.SEEDS.toString());
        this.boosters.getConfig().options().copyDefaults(true);
        //SAVES THE CONFIGURATION
        this.boosters.saveConfig();
        this.boosters.reloadConfig();
    }


    public void load(){
        //TODO REPLACE PLACEHOLDERS WITH VALUES
        this.SELL_ITEM = new ItemCreator(Material.valueOf(this.boosters.getConfig().getString("ItemSettings.SellBooster.ItemMaterial")))
                .setLore(this.boosters.getConfig().getStringList("ItemSettings.SellBooster.Lore"))
                .setName(ChatColor.translateAlternateColorCodes('&', this.boosters.getConfig().getString("ItemSettings.SellBooster.Name")))
                .toItemStack();

        this.EXP_ITEM = new ItemCreator(Material.valueOf(this.boosters.getConfig().getString("ItemSettings.ExpBooster.ItemMaterial")))
                .setLore(this.boosters.getConfig().getStringList("ItemSettings.ExpBooster.Lore"))
                .setName(ChatColor.translateAlternateColorCodes('&', this.boosters.getConfig().getString("ItemSettings.ExpBooster.Name")))
                .toItemStack();

        this.GROWTH_ITEM = new ItemCreator(Material.valueOf(this.boosters.getConfig().getString("ItemSettings.GrowthBooster.ItemMaterial")))
                .setLore(this.boosters.getConfig().getStringList("ItemSettings.GrowthBooster.Lore"))
                .setName(ChatColor.translateAlternateColorCodes('&', this.boosters.getConfig().getString("ItemSettings.GrowthBooster.Name")))
                .toItemStack();

        this.SQL_IP = this.boosters.getConfig().getString("Settings.SQL.ip");
        this.SQL_PORT = this.boosters.getConfig().getInt("Settings.SQL.port");
        this.SQL_DATABASE = this.boosters.getConfig().getString("Settings.SQL.database");
        this.SQL_USERNAME = this.boosters.getConfig().getString("Settings.SQL.username");
        this.SQL_PASSWORD = this.boosters.getConfig().getString("Settings.SQL.password");

    }
}
