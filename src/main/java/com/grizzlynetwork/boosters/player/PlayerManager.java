package com.grizzlynetwork.boosters.player;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.ActiveBooster;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private Boosters boosters;

    public PlayerManager(Boosters boosters) {
        this.boosters = boosters;
    }


    @Getter private Map<UUID, ActiveBooster> ACTIVE_EXP_BOOSTER = new HashMap<>(); //REPLACE INTEGER WITH ACTIVE BOOSTER OBJECT
    @Getter private Map<UUID, ActiveBooster> ACTIVE_SELL_BOOSTER = new HashMap<>();
    @Getter private Map<UUID, ActiveBooster> ACTIVE_GROWTH_BOOSTER = new HashMap<>();

    @Getter private Map<UUID, Double> EXP_BOOSTER_END_MILLIS = new HashMap<>();
    @Getter private Map<UUID, Double> SELL_BOOSTER_END_MILLIS = new HashMap<>();
    @Getter private Map<UUID, Double> GROWTH_BOOSTER_END_MILLIS = new HashMap<>();

    @Getter private Map<UUID, Double> EXP_BOOSTER_START_MILLIS = new HashMap<>();
    @Getter private Map<UUID, Double> SELL_BOOSTER_START_MILLIS = new HashMap<>();
    @Getter private Map<UUID, Double> GROWTH_BOOSTER_START_MILLIS = new HashMap<>();

}
