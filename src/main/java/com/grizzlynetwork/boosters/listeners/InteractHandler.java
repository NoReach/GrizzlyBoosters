package com.grizzlynetwork.boosters.listeners;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.ActiveBooster;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import com.grizzlynetwork.boosters.objects.MetaTypes;
import com.grizzlynetwork.boosters.util.TimeAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InteractHandler implements Listener {


    private Boosters boosters;

    public InteractHandler(Boosters boosters) {
        this.boosters = boosters;
    }


    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        ItemStack current = e.getItem();
        if (current != null) {
            if (this.boosters.getUtils().compareItemStacks(current, this.boosters.getConfigManager().getEXP_ITEM())) {
                activateBooster(e.getPlayer(), current);
            } else if (this.boosters.getUtils().compareItemStacks(current, this.boosters.getConfigManager().getGROWTH_ITEM())) {
                activateBooster(e.getPlayer(), current);
            } else if (this.boosters.getUtils().compareItemStacks(current, this.boosters.getConfigManager().getSELL_ITEM())) {
                activateBooster(e.getPlayer(), current);
            }

        }
    }



    private void activateBooster(Player player, ItemStack current)
    {
        BoosterTypes type = this.boosters.getUtils().getBoosterType(current);
        if (type == null){
            Bukkit.getLogger().severe("[Boosters] Booster type returned null in Utils method.");
            return;
        }
        double duration = new TimeAPI(this.boosters.getUtils().getFormattedDurFromItem(current)).getSeconds();
        double multiplier = this.boosters.getUtils().getMultiplierFromItem(current);
        ActiveBooster activeBooster = new ActiveBooster((int) multiplier, duration, type);
        double millis = new TimeAPI(this.boosters.getUtils().getFormattedDurFromItem(current)).getMilliseconds();
        switch (type){
            case GROWTH:
                if (this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().containsKey(player.getUniqueId())){
                    player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_ALREADY_ACTIVE());
                    return;
                }
                this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().put(player.getUniqueId(), activeBooster);
                this.boosters.getPlayerManager().getGROWTH_BOOSTER_END_MILLIS().put(player.getUniqueId(), System.currentTimeMillis() + millis);
                this.boosters.getPlayerManager().getGROWTH_BOOSTER_START_MILLIS().put(player.getUniqueId(), (double)System.currentTimeMillis());
                break;
            case SELL:
                if (this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().containsKey(player.getUniqueId())){
                    player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_ALREADY_ACTIVE());
                    return;
                }
                this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().put(player.getUniqueId(), activeBooster);
                this.boosters.getPlayerManager().getSELL_BOOSTER_END_MILLIS().put(player.getUniqueId(), System.currentTimeMillis() + millis);
                this.boosters.getPlayerManager().getSELL_BOOSTER_START_MILLIS().put(player.getUniqueId(), (double)System.currentTimeMillis());
                break;
            case EXP:
                if (this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().containsKey(player.getUniqueId())){
                    player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_ALREADY_ACTIVE());
                    return;
                }
                this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().put(player.getUniqueId(), activeBooster);
                this.boosters.getPlayerManager().getEXP_BOOSTER_END_MILLIS().put(player.getUniqueId(), System.currentTimeMillis() + millis);
                this.boosters.getPlayerManager().getEXP_BOOSTER_START_MILLIS().put(player.getUniqueId(), (double)System.currentTimeMillis());
        }
        player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_ACTIVATED()
                .replace("%TYPE%", type.toString())
                .replace("%MULTIPLIER%", String.valueOf(this.boosters.getUtils().getMultiplierFromItem(current)))
                .replace("%DURATION%", this.boosters.getUtils().getFormattedDurFromItem(current)));
        List<ItemStack> contents = Arrays.asList(player.getInventory().getContents());
        if (current.getAmount() > 1) {
            for (ItemStack itemStack : contents) {
                if (itemStack != null) {
                    if (itemStack.equals(current)) {
                        if (itemStack.getAmount() > 1) {
                            itemStack.setAmount(itemStack.getAmount() - 1);
                        }
                    }
                }
            }
        }else{
            player.getInventory().removeItem(current);
        }





    }
}
