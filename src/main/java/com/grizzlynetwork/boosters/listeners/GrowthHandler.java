package com.grizzlynetwork.boosters.listeners;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.ActiveBooster;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.material.Crops;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class GrowthHandler extends BukkitRunnable implements Listener{


    private Boosters boosters;

    public GrowthHandler(Boosters boosters) {
        this.boosters = boosters;
    }

    @Getter private Map<Island, List<Block>> islandBlocks = new HashMap<>();
    private int seconds = 0;

    @Override
    public void run() {
        seconds++;
        if (seconds % 5 == 0){
            this.islandBlocks.clear(); //EVERY 5 SECONDS CLEAR THE CACHE
        }
        for (UUID uuid : this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().keySet()){
            Player player = Bukkit.getPlayer(uuid);
            if (player == null) return;
            if (!player.isOnline()) return;
            ActiveBooster booster = this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().get(player.getUniqueId());
            if (booster.getTYPE() == BoosterTypes.GROWTH){
                Island island = ASkyBlockAPI.getInstance().getIslandOwnedBy(player.getUniqueId());
                int minX = island.getMinProtectedX();
                int minZ = island.getMinProtectedZ();
                int maxX = minX + (island.getProtectionSize() * 2);
                int maxZ = minZ + (island.getProtectionSize() * 2);
                Location cornerOne = new Location(island.getCenter().getWorld(), minX, 0, minZ);
                Location cornerTwo = new Location(island.getCenter().getWorld(), maxX, 150, maxZ);
                if (!this.islandBlocks.containsKey(island)){
                    List<Block> islandCrops = this.boosters.getUtils().blocksFromTwoPoints(cornerOne, cornerTwo);
                    this.islandBlocks.put(island, islandCrops);
                }
                this.islandBlocks.get(island).forEach(block ->
                {
                    if (block.getType() == Material.MOB_SPAWNER){
                        CreatureSpawner spawner = (CreatureSpawner) block.getState();
                        spawner.setDelay(spawner.getDelay() / booster.getMULTIPLIER());
                        spawner.update();
                    }
                });
            }
        }

    }


    @EventHandler
    public void onBlockGrow(BlockGrowEvent e){
        if (e.getBlock().getState().getData() instanceof Crops){
            Block block = e.getBlock();
            Island belongsTo = null;
            for (Map.Entry<Island, List<Block>> entrySet : this.islandBlocks.entrySet()){
                Island island = entrySet.getKey();
                List<Block> islandCrops = entrySet.getValue();
                if (islandCrops.contains(block)){
                    belongsTo = island;
                    break;
                }
            }
            if (belongsTo != null){
                belongsTo.getMembers().forEach(uuid -> {
                    if (this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().containsKey(uuid)){
                        ActiveBooster booster = this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().get(uuid);
                        if (booster.getTYPE() == BoosterTypes.GROWTH){
                            int multiplier = booster.getMULTIPLIER();
                            byte dataToSet = e.getNewState().getData().getData();
                            if (dataToSet * multiplier > 7){
                                dataToSet = (byte) 7;
                            }else{
                                dataToSet =  (byte) (dataToSet * (byte) multiplier);
                            }
                            this.boosters.log("Growth Booster for: " + uuid + " new data: " + dataToSet);
                            e.getNewState().getData().setData(dataToSet);
                        }
                    }
                });
            }
        }
    }

}
