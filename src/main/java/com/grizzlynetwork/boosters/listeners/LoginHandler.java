package com.grizzlynetwork.boosters.listeners;

import com.grizzlynetwork.boosters.Boosters;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LoginHandler implements Listener {

    private Boosters boosters;

    public LoginHandler(Boosters boosters) {
        this.boosters = boosters;
    }


    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        Player player = e.getPlayer();
        this.boosters.getSqlHandler().saveAllBoosters(player.getUniqueId());
    }

    @EventHandler
    public void onLogin(PlayerLoginEvent e){
        this.boosters.getSqlHandler().loadAllBoosters(e.getPlayer().getUniqueId());
    }
}
