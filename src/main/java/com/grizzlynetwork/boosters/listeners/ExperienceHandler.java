package com.grizzlynetwork.boosters.listeners;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

import java.util.logging.Level;

public class ExperienceHandler implements Listener {

    private Boosters boosters;

    public ExperienceHandler(Boosters boosters) {
        this.boosters = boosters;
    }

    @EventHandler
    public void onGain(PlayerExpChangeEvent e){
        Player player = e.getPlayer();
        int amountGained = e.getAmount();
        if (this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().containsKey(player.getUniqueId())){
            if (this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().get(player.getUniqueId()).getTYPE() == BoosterTypes.EXP){
                double newVal = amountGained * this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().get(player.getUniqueId()).getMULTIPLIER();
                e.setAmount((int) newVal);
                this.boosters.log("Multiplied Experience gained for player: " + player.getUniqueId() + " Old: " + amountGained + " New: " + newVal);
            }
            //double newVal = amountGained * this.boosters.getBoosterManager().getPlayerMultipliers().get(player.getUniqueId()); TODO UNCOMMENT AND CONSTRUCT AROUND PROJECT

        }
    }
}
