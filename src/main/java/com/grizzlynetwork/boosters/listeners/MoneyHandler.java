package com.grizzlynetwork.boosters.listeners;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import net.brcdev.shopgui.api.event.ShopPreTransactionEvent;
import net.brcdev.shopgui.shop.ShopManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class MoneyHandler implements Listener{


    private Boosters boosters;

    public MoneyHandler(Boosters boosters) {
        this.boosters = boosters;

    }


    @EventHandler
    public void onTransact(ShopPreTransactionEvent e){
        Player player = e.getPlayer();
        double amountGained = e.getPrice();
        if (e.getShopAction() == ShopManager.ShopAction.SELL || e.getShopAction() == ShopManager.ShopAction.SELL_ALL) {
            if (this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().containsKey(player.getUniqueId())){
                if (this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().get(player.getUniqueId()).getTYPE() == BoosterTypes.SELL){
                    double newVal = amountGained * this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().get(player.getUniqueId()).getMULTIPLIER();
                    e.setPrice(newVal);
                    this.boosters.log("Sell Booster for: " + player.getName() + " original amount: " +  amountGained + " new value: " + newVal);
                }

            }
        }
    }
}
