package com.grizzlynetwork.boosters.listeners;

import com.grizzlynetwork.boosters.Boosters;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlockHandler implements Listener {

    private Boosters boosters;

    public BlockHandler(Boosters boosters) {
        this.boosters = boosters;
    }


    @EventHandler
    public void onPlace(BlockPlaceEvent e){
        ItemStack stack = e.getItemInHand();
        if (this.boosters.getUtils().compareItemStacks(stack, this.boosters.getConfigManager().getEXP_ITEM())){
            e.setCancelled(true);
        }else if (this.boosters.getUtils().compareItemStacks(stack, this.boosters.getConfigManager().getGROWTH_ITEM())){
            e.setCancelled(true);
        }else if (this.boosters.getUtils().compareItemStacks(stack, this.boosters.getConfigManager().getSELL_ITEM())){
            e.setCancelled(true);
        }
    }


}
