package com.grizzlynetwork.boosters.objects;

import com.grizzlynetwork.boosters.Boosters;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Booster {


    @Getter @Setter private int amount = 0;
    @Getter @Setter private int multiplier = 0;
    @Getter @Setter private double duration_seconds = 0;
    @Getter private Player reciever = null;
    @Getter @Setter private BoosterTypes type;

    public Booster(Player reciever, BoosterTypes type, int amount, int multiplier, double duration_seconds){
        this.amount = amount;
        this.multiplier = multiplier;
        this.duration_seconds = duration_seconds;
        this.reciever = reciever;
        this.type = type;
    }

    public void give(Boosters boosters, String niceDuration){
        ItemStack toRecieve = null;
        switch (type){
            case EXP:
                toRecieve = boosters.getConfigManager().getEXP_ITEM().clone();
                break;
            case SELL:
                toRecieve = boosters.getConfigManager().getSELL_ITEM().clone();
                break;
            case GROWTH:
                toRecieve = boosters.getConfigManager().getGROWTH_ITEM().clone();
        }
        ItemMeta meta = toRecieve.getItemMeta();
        List<String> lore = meta.getLore();
        List<String> duplicateLore = new ArrayList<>();
        Duration duration = Duration.ofSeconds((long) this.duration_seconds);
        for (String string : lore){
            string = string.
                    replace("%TYPE%", type.toString()).
                    replace("%DURATION%", duration.toString().replace("PT", "")).
                    replace("%MULTIPLIER%", String.valueOf(this.multiplier));
            duplicateLore.add(string);
            boosters.log("Added string to new lore: " + string);
        }
        meta.setLore(duplicateLore);
        lore = null;
        duplicateLore = null;
        toRecieve.setItemMeta(meta);
        toRecieve.setAmount(this.amount);
        final ItemStack boosterItem = toRecieve.clone();
        this.reciever.getInventory().addItem(boosterItem);
        this.reciever.sendMessage(boosters.getMessageConfig().getGIVE_BOOSTER_MSG()
                .replace("%TYPE%", type.toString())
                .replace("%DURATION%", niceDuration)
                .replace("%MULTIPLIER%", String.valueOf(this.multiplier)));
        

    }
}
