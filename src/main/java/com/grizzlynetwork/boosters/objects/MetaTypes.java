package com.grizzlynetwork.boosters.objects;

public enum MetaTypes {

    DURATION,
    MULTIPLIER
}
