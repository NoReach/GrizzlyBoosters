package com.grizzlynetwork.boosters.objects;

public enum BoosterTypes {

    SELL,
    GROWTH,
    EXP
}
