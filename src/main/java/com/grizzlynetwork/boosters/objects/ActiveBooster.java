package com.grizzlynetwork.boosters.objects;

import lombok.Getter;
import lombok.Setter;

public class ActiveBooster {

    @Getter @Setter private int MULTIPLIER;
    @Getter @Setter private double DURATION_SECONDS;
    @Getter @Setter private BoosterTypes TYPE;


    public ActiveBooster(int multiplier, double duration, BoosterTypes type)
    {
        this.MULTIPLIER = multiplier;
        this.DURATION_SECONDS = duration;
        this.TYPE = type;
    }
}
