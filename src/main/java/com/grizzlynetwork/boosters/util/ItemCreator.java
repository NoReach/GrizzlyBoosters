package com.grizzlynetwork.boosters.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;


public class ItemCreator {

    private ItemStack item;

    public ItemCreator(Material material) {
        this(material, 1);
    }


    public ItemCreator(Material material, int amount) {
        item = new ItemStack(material, amount);
    }

    public ItemCreator setDurability(short dur) {
        item.setDurability(dur);
        return this;
    }

    public ItemCreator setName(String name) {
        if (name == null) {
            return this;
        }
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        item.setItemMeta(itemMeta);
        return this;
    }

    public ItemCreator addEnchant(Enchantment enchantment, int level) {
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.addEnchant(enchantment, level, true);
        item.setItemMeta(itemMeta);
        return this;
    }

    public ItemCreator setLore(List<String> lore) {
        if (lore == null) {
            return this;
        }
        List<String> toSet = new ArrayList<>();
        lore.forEach(string -> {
            toSet.add(ChatColor.translateAlternateColorCodes('&', string));
        });
        ItemMeta itemmeta = item.getItemMeta();
        itemmeta.setLore(toSet);
        item.setItemMeta(itemmeta);
        return this;
    }



    public ItemCreator setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    public ItemCreator setUnbreakable(boolean unbreakable) {
        ItemMeta meta = item.getItemMeta();
        meta.spigot().setUnbreakable(unbreakable);
        item.setItemMeta(meta);
        return this;
    }

    public ItemStack toItemStack(){
        return item;
    }


}