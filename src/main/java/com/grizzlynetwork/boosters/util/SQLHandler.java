package com.grizzlynetwork.boosters.util;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.ActiveBooster;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class SQLHandler {

    private Boosters boosters;

    public SQLHandler(Boosters boosters) {
        this.boosters = boosters;
        this.init();
    }


    private Connection connection;


    private void init(){
        try{
            this.boosters.log(this.boosters.getConfigManager().getSQL_PASSWORD());
            this.boosters.log(this.boosters.getConfigManager().getSQL_USERNAME());
            this.connection = DriverManager.getConnection("jdbc:mysql://45.35.193.34:3306/grizzly_data", "mc-admin", "*RD%uSnDRFv=xka7ym%R3A8?EV@PmgFP");
            Statement statement = this.connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `boosters_data` (" +
                    "  `uuid` VARCHAR(60)," +
                    "  `sell_booster` VARCHAR(60) DEFAULT 'N/A'," +
                    "  `growth_booster` VARCHAR(60) DEFAULT 'N/A'," +
                    "  `exp_booster` VARCHAR(60) DEFAULT 'N/A'" +
                    ");");
            Bukkit.getLogger().info("[Boosters] >> Successfully connected to the MySQL Database");
        }catch (SQLException e){
            e.printStackTrace();
            Bukkit.getLogger().info("[Boosters] >> Error whilst connecting to the MySQL database....");
        }
    }


    public void saveAllBoostersSync(UUID uuid){
        try {
            if (boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().containsKey(uuid)) {
                ActiveBooster GROWTH_BOOSTER = boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().get(uuid);
                double currentTime = System.currentTimeMillis();
                double endTime = boosters.getPlayerManager().getGROWTH_BOOSTER_END_MILLIS().get(uuid);
                long durationMillis = (long)endTime - (long)currentTime;
                double durationSeconds = TimeUnit.MILLISECONDS.toSeconds(durationMillis);
                PreparedStatement GROWTH_STATEMENT = connection.prepareStatement("INSERT INTO `boosters_data` (uuid, growth_booster) VALUES (?, ?)");
                GROWTH_STATEMENT.setString(1, uuid.toString());
                GROWTH_STATEMENT.setString(2, String.valueOf(GROWTH_BOOSTER.getMULTIPLIER()) + ":" + durationSeconds);
                GROWTH_STATEMENT.executeUpdate();
                GROWTH_STATEMENT.close();
            }
            if (boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().containsKey(uuid)) {
                ActiveBooster SELL_BOOSTER = boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().get(uuid);
                double currentTime = System.currentTimeMillis();
                double endTime = boosters.getPlayerManager().getSELL_BOOSTER_END_MILLIS().get(uuid);
                long durationMillis = (long)endTime - (long)currentTime;
                double durationSeconds = TimeUnit.MILLISECONDS.toSeconds(durationMillis);
                PreparedStatement SELL_STATEMENT = connection.prepareStatement("INSERT INTO `boosters_data` (uuid, sell_booster) VALUES (?, ?)");
                SELL_STATEMENT.setString(1, uuid.toString());
                SELL_STATEMENT.setString(2, String.valueOf(SELL_BOOSTER.getMULTIPLIER()) + ":" + durationSeconds);
                SELL_STATEMENT.executeUpdate();
                SELL_STATEMENT.close();
            }
            if (boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().containsKey(uuid)) {
                ActiveBooster EXP_BOOSTER = boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().get(uuid);
                double currentTime = System.currentTimeMillis();
                double endTime = boosters.getPlayerManager().getEXP_BOOSTER_END_MILLIS().get(uuid);
                long durationMillis = (long)endTime - (long)currentTime;
                double durationSeconds = TimeUnit.MILLISECONDS.toSeconds(durationMillis);
                PreparedStatement EXP_STATEMENT = connection.prepareStatement("INSERT INTO `boosters_data` (uuid, exp_booster) VALUES (?, ?)");
                EXP_STATEMENT.setString(1, uuid.toString());
                EXP_STATEMENT.setString(2, String.valueOf(EXP_BOOSTER.getMULTIPLIER()) + ":" + durationSeconds);
                EXP_STATEMENT.executeUpdate();
                EXP_STATEMENT.close();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void saveAllBoosters(UUID uuid){
        new BukkitRunnable(){
            @Override
            public void run(){
                try {
                    if (boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().containsKey(uuid)) {
                        ActiveBooster GROWTH_BOOSTER = boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().get(uuid);
                        double currentTime = System.currentTimeMillis();
                        double endTime = boosters.getPlayerManager().getGROWTH_BOOSTER_END_MILLIS().get(uuid);
                        long durationMillis = (long)endTime - (long)currentTime;
                        double durationSeconds = TimeUnit.MILLISECONDS.toSeconds(durationMillis);
                        PreparedStatement GROWTH_STATEMENT = connection.prepareStatement("INSERT INTO `boosters_data` (uuid, growth_booster) VALUES (?, ?)");
                        GROWTH_STATEMENT.setString(1, uuid.toString());
                        GROWTH_STATEMENT.setString(2, String.valueOf(GROWTH_BOOSTER.getMULTIPLIER()) + ":" + durationSeconds);
                        GROWTH_STATEMENT.executeUpdate();
                        GROWTH_STATEMENT.close();
                    }
                    if (boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().containsKey(uuid)) {
                        ActiveBooster SELL_BOOSTER = boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().get(uuid);
                        double currentTime = System.currentTimeMillis();
                        double endTime = boosters.getPlayerManager().getSELL_BOOSTER_END_MILLIS().get(uuid);
                        long durationMillis = (long)endTime - (long)currentTime;
                        double durationSeconds = TimeUnit.MILLISECONDS.toSeconds(durationMillis);
                        PreparedStatement SELL_STATEMENT = connection.prepareStatement("INSERT INTO `boosters_data` (uuid, sell_booster) VALUES (?, ?)");
                        SELL_STATEMENT.setString(1, uuid.toString());
                        SELL_STATEMENT.setString(2, String.valueOf(SELL_BOOSTER.getMULTIPLIER()) + ":" + durationSeconds);
                        SELL_STATEMENT.executeUpdate();
                        SELL_STATEMENT.close();
                    }
                    if (boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().containsKey(uuid)) {
                        ActiveBooster EXP_BOOSTER = boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().get(uuid);
                        double currentTime = System.currentTimeMillis();
                        double endTime = boosters.getPlayerManager().getEXP_BOOSTER_END_MILLIS().get(uuid);
                        long durationMillis = (long)endTime - (long)currentTime;
                        double durationSeconds = TimeUnit.MILLISECONDS.toSeconds(durationMillis);
                        PreparedStatement EXP_STATEMENT = connection.prepareStatement("INSERT INTO `boosters_data` (uuid, exp_booster) VALUES (?, ?)");
                        EXP_STATEMENT.setString(1, uuid.toString());
                        EXP_STATEMENT.setString(2, String.valueOf(EXP_BOOSTER.getMULTIPLIER()) + ":" + durationSeconds);
                        EXP_STATEMENT.executeUpdate();
                        EXP_STATEMENT.close();
                    }
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }

        }.runTaskAsynchronously(this.boosters);
    }


    /**
     * Method to load users boosters from SQL if they exist, once loaded set them back to none.
     * @param uuid
     */
    public synchronized void loadAllBoosters(UUID uuid){
        try{
            PreparedStatement statement = this.connection.prepareStatement("SELECT * FROM `boosters_data` WHERE `uuid` = ?");
            statement.setString(1, uuid.toString());
            statement.executeQuery();
            ResultSet rs = statement.getResultSet();
            while (rs.next()){
                String growth_booster = rs.getString("growth_booster");
                String sell_booster = rs.getString("sell_booster");
                String exp_booster = rs.getString("exp_booster");
                if (!growth_booster.equalsIgnoreCase("N/A")){
                    int multiplier = Integer.valueOf(growth_booster.split(":")[0]);
                    double duration_seconds = Double.valueOf(growth_booster.split(":")[1]);
                    ActiveBooster booster = new ActiveBooster(multiplier, duration_seconds, BoosterTypes.GROWTH);
                    this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().put(uuid, booster);
                    this.boosters.getPlayerManager().getGROWTH_BOOSTER_START_MILLIS().put(uuid, (double) System.currentTimeMillis());
                    this.boosters.getPlayerManager().getGROWTH_BOOSTER_END_MILLIS().put(uuid, (double) System.currentTimeMillis() + TimeUnit.SECONDS.toMillis((long) duration_seconds));
                    this.boosters.log("Found saved Growth Booster multiplier: " + multiplier + " duration remaining sec: " + duration_seconds);
                }
                if (!sell_booster.equalsIgnoreCase("N/A")){
                    int multiplier = Integer.valueOf(sell_booster.split(":")[0]);
                    double duration_seconds = Double.valueOf(sell_booster.split(":")[1]);
                    ActiveBooster booster = new ActiveBooster(multiplier, duration_seconds, BoosterTypes.SELL);
                    this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().put(uuid, booster);
                    this.boosters.getPlayerManager().getSELL_BOOSTER_START_MILLIS().put(uuid, (double) System.currentTimeMillis());
                    this.boosters.getPlayerManager().getSELL_BOOSTER_END_MILLIS().put(uuid, (double) System.currentTimeMillis() + TimeUnit.SECONDS.toMillis((long) duration_seconds));
                    this.boosters.log("Found saved sell Booster multiplier: " + multiplier + " duration remaining sec: " + duration_seconds);
                }
                if (!exp_booster.equalsIgnoreCase("N/A")){
                    int multiplier = Integer.valueOf(exp_booster.split(":")[0]);
                    double duration_seconds = Double.valueOf(exp_booster.split(":")[1]);
                    ActiveBooster booster = new ActiveBooster(multiplier, duration_seconds, BoosterTypes.EXP);
                    this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().put(uuid, booster);
                    this.boosters.getPlayerManager().getEXP_BOOSTER_START_MILLIS().put(uuid, (double) System.currentTimeMillis());
                    this.boosters.getPlayerManager().getEXP_BOOSTER_END_MILLIS().put(uuid, (double) System.currentTimeMillis() + TimeUnit.SECONDS.toMillis((long) duration_seconds));
                    this.boosters.log("Found saved exp Booster multiplier: " + multiplier + " duration remaining sec: " + duration_seconds);
                }

            }
            PreparedStatement deleteStatement = this.connection.prepareStatement("DELETE FROM `boosters_data` WHERE `uuid` = ?;");
            deleteStatement.setString(1, uuid.toString());
            deleteStatement.executeUpdate();
            deleteStatement.close();
            this.boosters.log("Reset User: " + uuid + " back to default saved values...");
            statement.close();
            rs.close();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    public void close(){
        try{
            this.connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
