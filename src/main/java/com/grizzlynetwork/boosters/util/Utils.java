package com.grizzlynetwork.boosters.util;

import com.grizzlynetwork.boosters.Boosters;
import com.grizzlynetwork.boosters.objects.BoosterTypes;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Utils {

    private Boosters boosters;

    public Utils(Boosters boosters) {
        this.boosters = boosters;
    }

    private final Pattern digitsOnly = Pattern.compile("[^\\d+]");



    public boolean compareItemStacks(ItemStack playerStack, ItemStack configItem)
    {
        if (playerStack.hasItemMeta() && configItem.hasItemMeta()){
            if (playerStack.getItemMeta().hasDisplayName()) {
                if (playerStack.getItemMeta().getDisplayName().equals(configItem.getItemMeta().getDisplayName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean compareWithLore(ItemStack stackOne, ItemStack stackTwo){
        if (stackOne == null){
            return false;
        }else if (stackTwo == null){
            return false;
        }
        return stackOne.isSimilar(stackTwo);
    }


    public boolean compareWithLoree(ItemStack stackOne, ItemStack stackTwo){
        if (stackOne == null || stackTwo == null) return false;
        if (stackOne.hasItemMeta() && stackTwo.hasItemMeta()){
            if (stackOne.getItemMeta().hasDisplayName() && stackTwo.getItemMeta().hasDisplayName()){
                if (stackOne.getItemMeta().getDisplayName().equalsIgnoreCase(stackTwo.getItemMeta().getDisplayName())){
                    if (stackOne.getItemMeta().hasLore() && stackTwo.getItemMeta().hasLore()){
                        if (stackOne.getItemMeta().getLore().toString().equals(stackTwo.getItemMeta().getLore().toString())){
                            this.boosters.log("Compared ItemStacks and found that they were identical with same lore and name");
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isBoosterItem(ItemStack itemStack){
        if (itemStack == null) return true;
        String EXP_NAME = this.boosters.getConfigManager().getEXP_ITEM().getItemMeta().getDisplayName();
        String SELL_NAME = this.boosters.getConfigManager().getSELL_ITEM().getItemMeta().getDisplayName();
        String GROWTH_NAME = this.boosters.getConfigManager().getGROWTH_ITEM().getItemMeta().getDisplayName();
        if (itemStack.hasItemMeta()){
            if (itemStack.getItemMeta().hasDisplayName()){
                if (itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(EXP_NAME)) return true;
                if (itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(SELL_NAME)) return true;
                if (itemStack.getItemMeta().getDisplayName().equalsIgnoreCase(GROWTH_NAME)) return true;
            }
        }
        return false;
    }

    public int getMultiplierFromItem(ItemStack itemStack)
    {
        if (itemStack.hasItemMeta()){
            if (itemStack.getItemMeta().hasLore()){
                List<String> lore = null;
                if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("sell")){
                    lore = this.boosters.getConfigManager().getSELL_ITEM().getItemMeta().getLore();
                }else if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("growth")){
                    lore = this.boosters.getConfigManager().getGROWTH_ITEM().getItemMeta().getLore();
                }else if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("exp")){
                    lore = this.boosters.getConfigManager().getSELL_ITEM().getItemMeta().getLore();
                }
                int defaultLoreindex = -1;
                for (String defLore : lore){
                    if (defLore.contains("Multiplier")){
                        defaultLoreindex = lore.indexOf(defLore);
                    }
                }
                this.boosters.log(">> STRIPED AND SPLIT" + ChatColor.stripColor(itemStack.getItemMeta().getLore().get(defaultLoreindex)).split(":").toString());
                String asString = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(defaultLoreindex)).split(":")[1];
                asString = digitsOnly.matcher(asString).replaceAll("");
                int multiplier = Integer.valueOf(asString);
                return multiplier;
            }
        }
        return -1;
    }


    public BoosterTypes getBoosterType(ItemStack itemStack){
        if (itemStack.hasItemMeta()){
            if (itemStack.getItemMeta().hasDisplayName()){
                if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("sell")){
                    return BoosterTypes.SELL;
                }else if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("growth")){
                    return BoosterTypes.GROWTH;
                }else if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("exp")){
                    return BoosterTypes.EXP;
                }else{
                    Bukkit.getLogger().info("Item does not match display name");
                }
            }
        }
        return null;
    }


    public String getFormattedDurFromItem(ItemStack itemStack){
        String duration = "";
        if (itemStack.hasItemMeta()){
            if (itemStack.getItemMeta().hasLore()){
                if (itemStack.getItemMeta().hasDisplayName()){
                    List<String> lore = null;
                    if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("sell")){
                        lore = this.boosters.getConfigManager().getSELL_ITEM().getItemMeta().getLore();
                    }else if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("growth")){
                        lore = this.boosters.getConfigManager().getGROWTH_ITEM().getItemMeta().getLore();
                    }else if (itemStack.getItemMeta().getDisplayName().toLowerCase().contains("exp")){
                        lore = this.boosters.getConfigManager().getSELL_ITEM().getItemMeta().getLore();
                    }
                    if (lore != null){
                        int defaultLoreDurIndex = -1;
                        for (String defLore : lore)
                        {
                            if (defLore.contains("Duration")){
                                defaultLoreDurIndex = lore.indexOf(defLore);
                            }
                        }
                        this.boosters.log(itemStack.getItemMeta().getLore().toString());
                        this.boosters.log(itemStack.getItemMeta().getLore().get(defaultLoreDurIndex));
                        String formattedLoreDur = ChatColor.stripColor(itemStack.getItemMeta().getLore().get(defaultLoreDurIndex).split(":")[1]);
                        duration = formattedLoreDur;
                    }
                }
            }
        }
        return duration;
    }


    public List<Block> blocksFromTwoPoints(Location loc1, Location loc2)
    {
        List<Block> blocks = new ArrayList<Block>();

        int topBlockX = (loc1.getBlockX() < loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());
        int bottomBlockX = (loc1.getBlockX() > loc2.getBlockX() ? loc2.getBlockX() : loc1.getBlockX());

        int topBlockY = (loc1.getBlockY() < loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());
        int bottomBlockY = (loc1.getBlockY() > loc2.getBlockY() ? loc2.getBlockY() : loc1.getBlockY());

        int topBlockZ = (loc1.getBlockZ() < loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());
        int bottomBlockZ = (loc1.getBlockZ() > loc2.getBlockZ() ? loc2.getBlockZ() : loc1.getBlockZ());

        for(int x = bottomBlockX; x <= topBlockX; x++)
        {
            for(int z = bottomBlockZ; z <= topBlockZ; z++)
            {
                for(int y = bottomBlockY; y <= topBlockY; y++)
                {
                    Block block = loc1.getWorld().getBlockAt(x, y, z);
                    if (block.getType() == Material.AIR) continue;
                    if (block.getType() != Material.CROPS && block.getType() != Material.MOB_SPAWNER) continue;;
                    if (block.getState().getData() instanceof Crops){
                        blocks.add(block);
                    }else if (block.getType() == Material.MOB_SPAWNER){
                        blocks.add(block);
                    }
                }
            }
        }
        return blocks;
    }




}

