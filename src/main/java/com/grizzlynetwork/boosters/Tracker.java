package com.grizzlynetwork.boosters;

import com.grizzlynetwork.boosters.objects.BoosterTypes;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Map;
import java.util.UUID;

public class Tracker extends BukkitRunnable {

    private Boosters boosters;

    public Tracker(Boosters boosters) {
        this.boosters = boosters;
    }


    @Override
    public void run() {
        for (Map.Entry<UUID, Double> entrySet : this.boosters.getPlayerManager().getSELL_BOOSTER_END_MILLIS().entrySet()){
            UUID uuid = entrySet.getKey();
            double endTime = entrySet.getValue();
            if (System.currentTimeMillis() >= endTime){
                Player player = Bukkit.getPlayer(uuid);
                if (player == null) return;
                player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_EXPIRED()
                .replace("%TYPE%", BoosterTypes.SELL.toString()));
                this.boosters.getPlayerManager().getACTIVE_SELL_BOOSTER().remove(player.getUniqueId());
                this.boosters.getPlayerManager().getSELL_BOOSTER_END_MILLIS().remove(uuid);
            }
        }
        for (Map.Entry<UUID, Double> entrySet : this.boosters.getPlayerManager().getEXP_BOOSTER_END_MILLIS().entrySet()){
            UUID uuid = entrySet.getKey();
            double endTime = entrySet.getValue();
            if (System.currentTimeMillis() >= endTime){
                Player player = Bukkit.getPlayer(uuid);
                if (player == null) return;
                player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_EXPIRED()
                        .replace("%TYPE%", BoosterTypes.EXP.toString()));
                this.boosters.getPlayerManager().getACTIVE_EXP_BOOSTER().remove(player.getUniqueId());
                this.boosters.getPlayerManager().getEXP_BOOSTER_END_MILLIS().remove(uuid);
            }
        }
        for (Map.Entry<UUID, Double> entrySet : this.boosters.getPlayerManager().getGROWTH_BOOSTER_END_MILLIS().entrySet()){
            UUID uuid = entrySet.getKey();
            double endTime = entrySet.getValue();
            if (System.currentTimeMillis() >= endTime){
                Player player = Bukkit.getPlayer(uuid);
                if (player == null) return;
                player.sendMessage(this.boosters.getMessageConfig().getBOOSTER_EXPIRED()
                        .replace("%TYPE%", BoosterTypes.GROWTH.toString()));
                this.boosters.getPlayerManager().getACTIVE_GROWTH_BOOSTER().remove(player.getUniqueId());
                this.boosters.getPlayerManager().getGROWTH_BOOSTER_END_MILLIS().remove(uuid);
            }
        }

    }
}
